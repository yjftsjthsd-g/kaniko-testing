# kaniko-testing

This repo exists so that I can figure out how to use kaniko, and for any future
testing/experiments I might want to run.

## License

This repo itself is under CC0 (see LICENSE), though its outputs incorporate
works under other licenses.

