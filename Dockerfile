FROM alpine:latest
#ADD --chmod=555 hello.sh /usr/local/bin/hello
ADD hello.sh /usr/local/bin/hello
RUN chmod 555 /usr/local/bin/hello
ENTRYPOINT hello
